class User:

    def __init__(self, client_id):
        self.statuses = dict(
            menu = 1,
            settings = 2,
            settings_day_limit = 3,
            settings_speed = 4,
            settings_auth = 5,
            settings_channel = 6,
            task = 7,
            exc_wait = 8
        )
        
        self.client_id = client_id
        self.status = self.statuses["menu"]
    
    def get_status(self):
        return self.status
    
    def set_client_id(self, client_id):
        self.client_id = client_id
    
    def set_status(self, status):
        self.status = self.statuses[status]