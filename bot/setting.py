import json

class Settings:

    def __init__(self):
        self.settings = {}
        with open('settings.json', 'r', encoding='utf-8') as f:
            self.settings = json.load(f)
    
    def get(self, key, default=None):
        return self.settings.get(key, default)
    
    def set(self, key, value):
        self.settings[key] = value

        with open('settings.json', 'w', encoding='utf-8') as f:
            json.dump(self.settings, f, indent=4, ensure_ascii=False)