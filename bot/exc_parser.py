import pandas
import os
import re
import json

from telebot.types import Message
from telebot.async_telebot import AsyncTeleBot

PHONE_REGEX = r"^[\+]?[0-9]{11,13}$"

async def telegram_download(bot: AsyncTeleBot, file_id: str):
    created_file = await bot.get_file(file_id)
    content = await bot.download_file(created_file.file_path)

    with open('file.xlsx', 'wb') as f:
        f.write(content)

def google_download(url: str):
    pass

def yandex_download(url: str):
    pass

def delete_file():
    os.remove('file.xlsx')

async def download(bot: AsyncTeleBot, message: Message):
    
    if message.document is not None:
        await telegram_download(bot, message.document.file_id)
        return
    
    if message.text is not None:
        if message.text.find("yandex") != -1:
            yandex_download(message.text)
            return
        if message.text.find("google") != -1:
            google_download(message.text)
            return

    return Exception

async def ex_parse(bot: AsyncTeleBot, message: Message):
    await download(bot, message)

    df = pandas.read_excel('file.xlsx')

    phone_numbers = {}

    def get_phone_numbers(cell: str):
        cell = str(cell).replace(' ', '').replace('(', '').replace(')', '').replace('-', '').replace('.', '')
        match = re.match(PHONE_REGEX, cell)
        if match is not None:
            phone_numbers[match[0]] = {}
        return cell

    df.applymap(lambda cell: get_phone_numbers(cell))

    with open('users.json', 'w', encoding='utf-8') as f:
        json.dump(phone_numbers, f)
    
    delete_file()
