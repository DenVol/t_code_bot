from telethon.tl.functions.channels import InviteToChannelRequest
from telethon.tl.functions.contacts import ImportContactsRequest, DeleteContactsRequest
from telethon.tl.types import InputPhoneContact, Channel, PeerUser
from telethon import TelegramClient

from qrcode import QRCode

from .setting import Settings

class Client:
    def __init__(self, settings: Settings):
        self.client = TelegramClient('first_session', settings.get('application_id'), settings.get('application_hash'), timeout=600)
        self.qr = None

    async def is_connected(self):
        return await self.client.is_connected()
    
    async def is_authorized(self):
        return await self.client.is_user_authorized()

    async def connect(self):
        return await self.client.connect()
    
    async def disconnect(self):
        return await self.client.disconnect()
    
    async def authorize(self):
        self.qr = await self.client.qr_login()
        self.generate_qr()

    async def wait_authorize(self):
        await self.qr.wait(timeout=300)

    def generate_qr(self):
        qr = QRCode()
        qr.add_data(self.qr.url)
        qr.make()
        
        img = qr.make_image()
        with open('file.img', 'wb') as f:
            img.save(f)
        
    async def get_channel_by_name(self, channel_name):
        dialogs = await self.client.get_dialogs()
        for dialog in dialogs:
            if isinstance(dialog.entity, Channel):
                if dialog.entity.title == channel_name:
                    if dialog.entity.admin_rights.invite_users:
                        return {'name': dialog.entity.title, 'id': dialog.entity.id}
        return None
    
    async def invite_to_channel(self, channel_id, user_id):
        await self.client(InviteToChannelRequest(
            channel=channel_id,
            users=[user_id]
        ))

        await self.client(DeleteContactsRequest([user_id]))
    
    async def send_message(self, user_id, message):
        await self.client.send_message(PeerUser(user_id), message)