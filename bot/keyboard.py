import json
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

def get_messages() -> dict:
    messages = {}
    with open('messages.json', 'r', encoding='utf-8') as messages:
        messages = json.load(messages)
    return messages
    
def main_keyboard():
    messages = get_messages()
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)

    start_stop_btn = KeyboardButton(messages["start_stop_btn"])
    settgins_btn = KeyboardButton(messages["settings_btn"])
    exc_btn = KeyboardButton(messages["exc_btn"])
    # file_load_btn = KeyboardButton(messages["file_load_btn"])
    
    keyboard.add(start_stop_btn)
    keyboard.add(settgins_btn)
    keyboard.add(exc_btn)
    # keyboard.add(file_load_btn)

    return keyboard

def setting_keyboard():
    messages = get_messages()
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)

    day_limit_btn = KeyboardButton(messages["day_limit_btn"])
    speed_limit_btn = KeyboardButton(messages["speed_limit_btn"])
    channel_btn = KeyboardButton(messages["channel_btn"])
    authorized_btn = KeyboardButton(messages["authorized_btn"])
    home_btn = KeyboardButton(messages["home_btn"])

    keyboard.add(day_limit_btn)
    keyboard.add(speed_limit_btn)
    keyboard.add(channel_btn)
    keyboard.add(authorized_btn)
    keyboard.add(home_btn)

    return keyboard