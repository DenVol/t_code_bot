import asyncio
import datetime
import json
import os

from time import time
from .setting import Settings
from .client_api import Client

class Process:
    def __init__(self, client_api: Client, settings: Settings):
        self.client = client_api
        self.task = None
        self.speed_limit = settings.get('speed_limit')
        self.day_limit = settings.get('day_limit')
        self.channel_id = settings.get('channel', {"id": 0})['id']

        self.data = {}
        with open('users.json', 'r', encoding='utf-8') as f:
            self.data = json.load(f)
        
        self.meta = {}
        if os.path.exists('meta.json'):
            with open('meta.json', 'r', encoding='utf-8') as f:
                self.meta = json.load(f)
        

    def task_is_execute(self):
        if self.task:
            return not (self.task.done() or self.task.canceled())
        return False

    def write_meta(self, phone, first_name, last_name, status):
        self.meta[phone] = {
            'name': f'{first_name} {last_name}',
            'phone': phone,
            'status': status
        }

        with open('meta.json', 'w', encoding='utf-8') as f:
            json.dump(self.meta, f, indent=4, ensure_ascii=False)
    
    def start_task(self, callback):
        self.taks = asyncio.ensure_future(self._sync_task)
        self.task.add_done_callback(callback)
    
    async def stop_task(self):
        if self.task is not None and not self.task.cancelled():
            await self.client.disconnect()
            self.task.cancel()
        else:
            self.task = None
    
    async def _sync_task(self):
        self.start_time = time()
        await self.client.connect()
        count = 0

        el_number = len(self.data.keys())
        first_name = 'name'
        last_name_num = count

        for phone, value in self.data.items():
            if self.meta.get(phone) is not None:
                el_number -= 1
                continue
        
            last_name = str(last_name_num)
            try:
                client_id = await self.client.add_user_to_contacts(phone, first_name, last_name)
                await self.client.invite_to_channel(self.channel_id, client_id)
                self.write_meta(phone, first_name, last_name, 1)
            except Exception as e:
                self.write_meta(phone, first_name, last_name, 0)
            
            count += 1
            last_name_num += 1

            if count == self.day_limit:
                await self.client.disconnect()
                await self._wait_next_day()
                el_number -= count
                count = 0
                await self.client.connect()
            elif count == el_number:
                break
            else:
                await asyncio.sleep(self.speed_limit * 60)
        await self.client.disconnect()
        
    async def _wait_next_day(self):
        cuurent_date = time()
        next_day = (datetime.datetime.fromtimestamp(self.start_time) + datetime.timedelta(days=1)).timestamp()
        wait_time = next_day - cuurent_date
        await asyncio.sleep(wait_time) 