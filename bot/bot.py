import asyncio

from asyncio import exceptions
from telebot.types import Message
from telebot.async_telebot import AsyncTeleBot

from .keyboard import main_keyboard, setting_keyboard, get_messages
from .setting import Settings
from .client_api import Client
from .users import User
from .process import Process
from .exc_parser import ex_parse

def validate_day_limit(day_limit: int) -> bool:
    return day_limit > 0 and day_limit <= 200

def validate_speed(speed: int) -> bool:
    return speed > 0 and speed <= 20

def main():
    settings = Settings()
    client_api = Client(settings)
    process = Process(client_api, settings)

    messages = get_messages()
    user = User(0)

    bot = AsyncTeleBot(settings.get('bot_token'))

    @bot.message_handler(commands=['help', 'start'])
    async def start_handler(message: Message):
        user.set_client_id(message.from_user.id)
        await bot.send_message(message.chat.id, text=messages["start_msg"])
        await home_handler(message)
    
    @bot.message_handler(func=lambda msg: msg.text is not None and (msg.text == '/home' or msg.text == messages["home_btn"]))
    async def home_handler(message: Message):
        await client_api.connect()
        settings.set('auth', await client_api.is_authorized())
        await client_api.disconnect()

        user.set_status('menu')
        await bot.send_message(message.chat.id, text=messages["home_msg"], reply_markup=main_keyboard())
    
    async def actual_settings(message: Message):
        authorized = settings.get('auth', default=False)
        msg = "Авторизован" if authorized else "Не авторизован"
        await bot.send_message(
            message.chat.id,
            messages['actual_settings'].format(
                settings.get('day_limit'),
                settings.get('speed_limit'),
                msg,
                settings.get('channel', {'name': 'Нет канала'})['name']
            )
        )


    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["settings_btn"])
    async def settings_handler(message: Message):
        user.set_status('settings')

        await actual_settings(message)
        await bot.send_message(message.chat.id, text=messages["settings_msg"], reply_markup=setting_keyboard())

    
    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages['day_limit_btn'])
    async def settings_day_limit_handler(message: Message):
        user.set_status('settings_day_limit')
        await bot.send_message(message.chat.id, messages["day_limit"])

    @bot.message_handler(func=lambda _: user.get_status() == user.statuses["settings_day_limit"])
    async def edit_day_limit_handler(message: Message):
        day_limit = message.text

        if not day_limit.isdigit() and not validate_day_limit(int(day_limit)):
            msg = await bot.reply_to(message, messages['day_limit_error'])
            return

        settings.set('day_limit', int(day_limit))
        await settings_handler(message)
    
    
    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["speed_limit_btn"])
    async def settings_speed_limit_handler(message: Message):
        user.set_status('settings_speed')
        await bot.send_message(message.chat.id, messages['speed_limit'])
    
    @bot.message_handler(func=lambda _: user.get_status() == user.statuses["settings_speed"])
    async def edit_speed_limit_handler(message: Message):
        speed_limit = message.text

        if not speed_limit.isdigit() and not validate_speed(int(speed_limit)):
            await bot.send_message(message, messages['speed_limit_error'])
        
        settings.set('speed_limit', int(speed_limit))
        await settings_handler(message)

    
    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages['authorized_btn'])
    async def settings_authorize_handler(message: Message):
        user.set_status('settings_auth')
        
        await bot.send_message(message.chat.id, messages['auth_msg'])
        await client_api.connect()
        await client_api.authorize()

        with open('file.img', 'rb') as f:
            await bot.send_photo(message.chat.id, f)
        
        try:
            await client_api.wait_authorize()
        except exceptions.TimeoutError:
            await bot.send_message(message.chat.id, messages['auth_timeout'])
            await settings_handler(message)
            return

        settings.set('auth', True)
        await bot.send_message(message.chat.id, messages['auth_success'])
        await client_api.disconnect()

        await settings_handler(message)


    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages['channel_btn'])
    async def settings_channel_handler(message: Message):
        user.set_status('settings_channel')
        await bot.send_message(message.chat.id, messages['channel'])
    
    @bot.message_handler(func=lambda _: user.get_status() == user.statuses["settings_channel"])
    async def edit_channel_settings_handler(message: Message):
        await client_api.connect()
        channel_name = message.text
        channel = await client_api.get_channel_by_name(channel_name)
        if channel:
            settings.set('channel', channel)
            await settings_handler(message)
            return
        
        await bot.send_message(message.chat.id, messages['channel_error'])

    # @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["start_stop_btn"])
    # async def start_stop_handler(message: Message):
    #     if not process.task_is_execute():
    #         user.set_status('task')
    #         await bot.send_message(message.chat.id, messages["start_task"])
    #         process.start_task(lambda _: asyncio.ensure_future(bot.send_message(message.chat.id, messages["task_is_done"])))
    #         await bot.send_message(message.chat.id, messages["start_task_success"])
    #     else:
    #         user.set_status('menu')
    #         await process.cancel()
    #         await bot.send_message(message.chat.id, messages['stop_task_success'])

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["exc_btn"])
    async def excell_parse_start(message: Message):
        await bot.send_message(message.chat.id, messages['send_exc_file'])
        user.set_status('exc_wait')

    @bot.message_handler(content_types=['text', 'document'], func=lambda _: user.get_status() == user.statuses["exc_wait"])
    async def excell_parse(message: Message):
        await ex_parse(bot, message)
        await bot.send_message(message.chat.id, messages['exc_file_parse_success'])
        user.set_status('menu')
        await home_handler(message)

    asyncio.get_event_loop().run_until_complete(bot.polling())