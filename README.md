# t_code_bot

Работу подготовил студент АСУб-19-1 Вологдин Денис
Цель проекта - создать бота, с помощью которого можно осуществлять рассылки приглашений в определённый канал

<!-- # Инструкция по использованию

- Запускаем бота
- Загружаем Excel-файл или кидаем ссылку на него
- Настраиваем рассылку
- Ожидаем
- Получаем оповещение об окончании рассылки -->

# Документация

## Алгоритм работы бота
Примерный алгоритм работы бота описан на следующем рисунке
![](bot_algo.png)